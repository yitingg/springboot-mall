package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CreateOrderRequest;
import com.example.demo.dto.OrderQueryParams;
import com.example.demo.model.Order;
import com.example.demo.service.OrderService;
import com.example.demo.util.Page;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;


@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@GetMapping("/users/{userId}/orders")
	public ResponseEntity<Page<Order>> getOrders(
			@PathVariable Integer userId,
			@RequestParam(defaultValue = "10") @Max(1000) @Min(0) Integer limit,
			@RequestParam(defaultValue = "0") @Min(0) Integer offset){
		
		OrderQueryParams oQP = new OrderQueryParams();
		oQP.setUserId(userId);
		oQP.setLimit(limit);
		oQP.setOffset(offset);
		
		//取得orderList
		List<Order> orderList = orderService.getOrders(oQP);
		
		//取得orderTotalAmount
		Integer count = orderService.countOrder(oQP);
		
		//分頁
		Page<Order> p = new Page<>();
		p.setLimit(limit);
		p.setOffset(offset);
		p.setTotal(count);
		p.setResult(orderList);
		
		return ResponseEntity.status(HttpStatus.OK).body(p);
	}		
	
	
	@PostMapping("/users/{userId}/orders") 
	public ResponseEntity<?> createOrder(@PathVariable Integer userId, 
										 @RequestBody @Valid CreateOrderRequest createOrderRequest){
		
		Integer orderId = orderService.createOrder(userId, createOrderRequest);
		
		Order order = orderService.getOrderById(orderId);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(order);
	}
}
