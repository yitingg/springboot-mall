package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserLoginRequest;
import com.example.demo.dto.UserRegisterRequest;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

import jakarta.validation.Valid;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	
	@PostMapping("/users/register") 
	//register方法使用post的理由
	//1.RESTful中 創建資源對應到POST方法
	//2.資安考量 需要使用 request body 傳遞參數 (以資安考量為主)
	public ResponseEntity<User> register(@RequestBody @Valid UserRegisterRequest uRR){
		Integer userId = userService.register(uRR);
		
		User user = userService.getUserById(userId);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(user);
	}
	
	@PostMapping("/users/login")
	//@RequestBody接住前端所傳過來的參數，@Valid 驗證post請求requestbody 的參數
	public ResponseEntity<User> login(@RequestBody @Valid  UserLoginRequest userLoginRequest) {
		//使用者login的密碼，是否能夠登入，login方法會回傳user類型的返回值回來
		User user = userService.login(userLoginRequest);
		
		return ResponseEntity.status(HttpStatus.OK).body(user);
	}
}
