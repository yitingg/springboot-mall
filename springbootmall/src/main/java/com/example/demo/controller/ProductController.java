package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constant.ProductCategory;
import com.example.demo.dto.ProductQueryParams;
import com.example.demo.dto.ProductRequest;
import com.example.demo.model.Product;
import com.example.demo.service.ProductService;
import com.example.demo.util.Page;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

@Validated
@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	
	@GetMapping("/products") //查找全部商品
	//@RequestParam(required = false)由於category是可選參數 所以前端在request時就不一定要加上 沒有加上category值就為null
	public ResponseEntity<Page<Product>> getProducts(
			//篩選條件 filtering
			@RequestParam(required = false)  ProductCategory category,
			@RequestParam(required = false)  String search,
			
			//排序 sorting
			@RequestParam(defaultValue = "created_date")  String orderBy,
			@RequestParam(defaultValue = "desc")  String sort,
	
			//分頁 pagination => 保護後端存取資料的效能
			@RequestParam(defaultValue = "5") @Max(1000) @Min(0) Integer limit, 
			@RequestParam(defaultValue = "0") @Min(0) Integer offset 
			//limit限制只要幾筆資料,offset跳過表中前?筆數據,舉例:LIMIT 2 OFSET 3 (跳過前三筆後取兩筆資料)
			//@Max,@Min為4-13教得驗證請求參數,記得要手動在class前加上@Validated才會生效
			){
		ProductQueryParams pqp = new ProductQueryParams(); //把前端傳過來的值set到pqp中,由service一路傳至dao
		pqp.setCategory(category);
		pqp.setSearch(search);
		pqp.setOrderBy(orderBy);
		pqp.setSort(sort);
		pqp.setLimit(limit);
		pqp.setOffset(offset);
		
		//取得 product list
		List<Product> plist = productService.getProducts(pqp);
		
		//取得 product 總數
		Integer total = productService.countProduct(pqp); //加上pqp是因為商品的總數會隨著篩選條件的變化有所增減
		
		//設定分頁要用到的值
		Page<Product> page = new Page<>();
		page.setLimit(limit);
		page.setOffset(offset);
		page.setTotal(total);
		page.setResult(plist);
		
		return ResponseEntity.status(HttpStatus.OK).body(page);
	}

	
	@GetMapping("/products/{productId}") //用ID查找單一商品
	public ResponseEntity<Product> getProduct(@PathVariable Integer productId){
		Product product = productService.getProductById(productId);
		
		if (product != null) {
			return ResponseEntity.status(HttpStatus.OK).body(product);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); //回傳response回前端
	}
	
	@RequestMapping("/products") //新增商品
	public ResponseEntity<Product> createProduct(@RequestBody @Valid ProductRequest productRequest){
		//@RequestBody接住前端船傳回來的參數, 只要有在ProductRequest中使用NotNull註解要在前面加上@Valid才會生效
	
		Integer productId = productService.createProduct(productRequest);
		
		Product product = productService.getProductById(productId);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(product);
	}
	
	@PutMapping("/products/{productId}") //修改商品
	public ResponseEntity<Product> updateProduct(@PathVariable Integer productId,
												@RequestBody @Valid ProductRequest productRequest){
		
		//檢查product是否存在
		Product product = productService.getProductById(productId);
		
		if (product == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		//修改商品的數據
		productService.updateProduct(productId, productRequest);
		
		Product updatedProduct = productService.getProductById(productId);
		
		return ResponseEntity.status(HttpStatus.OK).body(updatedProduct);
	}
	
	//刪除
	@DeleteMapping("/products/{productId}")
	public ResponseEntity<?> deleteProduct(@PathVariable Integer productId){
		productService.deleteProductById(productId);
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
}
