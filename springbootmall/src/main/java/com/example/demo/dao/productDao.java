package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.ProductQueryParams;
import com.example.demo.dto.ProductRequest;
import com.example.demo.model.Product;

public interface productDao {

	Integer countProduct(ProductQueryParams pqp);
	
	List<Product> getProducts(ProductQueryParams pqp);
	
	Product getProductById(Integer productId); //根據productId查詢商品數據
	
	Integer createProduct(ProductRequest productrequest);
	
	void updateStock(Integer productId, Integer stock);
	
	void updateProduct(Integer productId, ProductRequest productRequest);
	
	void deleteProductById(Integer productId);
}
