package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.OrderQueryParams;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;


public interface OrderDao {
	
	Integer countOrder(OrderQueryParams oQP);
	
	List<Order> getOrders(OrderQueryParams oQP);
	
	Order getOrderById(Integer orderId);
	
	List<OrderItem> getOrderItemsByOrderId(Integer orderId); //一個訂單會包含多個商品=>List

	Integer createOrder(Integer userId, Integer totalAmount);

	void createOrderItems(Integer orderId, List<OrderItem> orderItemList);
}
