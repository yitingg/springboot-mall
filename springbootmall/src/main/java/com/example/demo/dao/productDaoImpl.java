package com.example.demo.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.example.demo.dto.ProductQueryParams;
import com.example.demo.dto.ProductRequest;
import com.example.demo.model.Product;
import com.example.demo.rowmapper.ProductRowMapper;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Component
public class productDaoImpl implements productDao{  //Dao層只跟資料庫溝通不會有複雜的判斷邏輯(判斷放在Service層)

	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public Integer countProduct(ProductQueryParams pqp) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("SELECT count(*) FROM product WHERE 1=1");

		Map<String, Object> map = new HashMap<>();
		
//		if (pqp.getCategory() != null) {
//			sqlB.append(" AND category = :category");  
//			map.put("category", pqp.getCategory().name());
//		}
//		
//		if (pqp.getSearch() != null) {
//			sqlB.append(" AND product_name LIKE :search");  
//			map.put("search", "%" + pqp.getSearch() + "%"); //LIKE搭配%(任意字符)使用%記得加在sql語法外
//		} 
		
		sqlB = addFilteringSql(sqlB, map, pqp);
		
		Integer total = namedParameterJdbcTemplate.queryForObject(sqlB.toString(), map, Integer.class);
		
		return total;
	}
	
	
	//查詢全部商品
	//對商品篩選及排序如http://localhost:8080/products?orderBy=price&sort=desc&category=CAR
	//在拼接資料庫語法時記得要檢查空白
	@Override
	public List<Product> getProducts(ProductQueryParams pqp) {
		//WHERE1=1一定會過只是為了讓下面其他的查詢條件能隨意拼接在sql語句後面
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("SELECT product_id,product_name, category, image_url, price, stock, description, ");
		sqlB.append("created_date, last_modified_date FROM product WHERE 1=1");
		
		Map<String, Object> map = new HashMap<>();
		
		//查詢條件
//		if (pqp.getCategory() != null) {
//			sqlB.append(" AND category = :category");  
//			map.put("category", pqp.getCategory().name());
//		}
//		
//		if (pqp.getSearch() != null) {
//			sqlB.append(" AND product_name LIKE :search");  
//			map.put("search", "%" + pqp.getSearch() + "%"); //LIKE搭配%(任意字符)使用%記得加在sql語法外
//		}
		
		sqlB = addFilteringSql(sqlB, map, pqp);
		
		//排序 這裡不用下if來判斷null是因為在controller中已經設過要排序的defaultValue了
		sqlB.append(" ORDER BY " + pqp.getOrderBy() + " " + pqp.getSort());
		
		//分頁 不加if判斷null理由同上
		sqlB.append(" LIMIT :limit OFFSET :offset");
		map.put("limit", pqp.getLimit());
		map.put("offset", pqp.getOffset());
		
		List<Product> plist =  namedParameterJdbcTemplate.query(sqlB.toString(), map, new ProductRowMapper());
		
		return plist;
	}
	
	//ID查詢商品
	@Override
	public Product getProductById(Integer productId) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("SELECT product_id,product_name, category, image_url, price, stock, description, ");
		sqlB.append("created_date, last_modified_date ");
		sqlB.append("FROM product WHERE product_id = :productId ");
//		String sql = "SELECT product_id,product_name, category, image_url, price, stock, description, "
//				+ "created_date, last_modified_date"
//				+ "FROM product WHERE product_id = :productId";
//		log.info("AntTest=>"+sqlB.toString());
		Map<String, Object> map = new HashMap<>();
		map.put("productId", productId);
		
		List<Product> productList =  namedParameterJdbcTemplate.query(sqlB.toString(), map, new ProductRowMapper());
		
		if(productList.size() > 0) {
			return productList.get(0);
		}else {
			return null;
		}	
	}
	
	
	//新增商品
	@Override
	public Integer createProduct(ProductRequest productRequest) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("INSERT INTO product(product_name, category, image_url, price, stock, ");
		sqlB.append("description, created_date, last_modified_date)");
		sqlB.append("VALUES (:productname, :category, :imageurl, :price, :stock, :description, ");
		sqlB.append(":createddate, :lastmodifieddate)");
		
		Map<String,Object> map = new HashMap<>();
		map.put("productname", productRequest.getProductName());
		map.put("category", productRequest.getCategory().toString());
		map.put("imageurl", productRequest.getImageUrl());
		map.put("price", productRequest.getPrice());
		map.put("stock", productRequest.getStock());
		map.put("description", productRequest.getDescription());
		
		Date now = new Date();
		map.put("createddate", now);
		map.put("lastmodifieddate", now);
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		namedParameterJdbcTemplate.update(sqlB.toString(), new MapSqlParameterSource(map), keyHolder);
		int productId = keyHolder.getKey().intValue();
		return productId;
		
	}
	
	//更新商品
	@Override
	public void updateProduct(Integer productId, ProductRequest productRequest) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("UPDATE product SET product_name = :productName, category = :category, ");
		sqlB.append("image_url = :imageUrl, price = :price, stock = :stock, description = :description, ");
		sqlB.append("last_modified_date = :lastModifiedDate WHERE product_id = :productId ");

		Map<String,Object> map = new HashMap<>();
		map.put("productId", productId);
		
		map.put("productName", productRequest.getProductName());
		map.put("category", productRequest.getCategory().toString());
		map.put("imageUrl", productRequest.getImageUrl());
		map.put("price", productRequest.getPrice());
		map.put("stock", productRequest.getStock());
		map.put("description", productRequest.getDescription());
		
		map.put("lastModifiedDate", new Date());
		
		namedParameterJdbcTemplate.update(sqlB.toString(), map);
	}
	
	@Override
	public void updateStock(Integer productId, Integer stock) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("UPDATE product SET  stock = :stock, last_modified_date = :lastModifiedDate ");
		sqlB.append("WHERE product_id = :productId ");
		
		Map<String,Object> map = new HashMap<>();
		map.put("productId", productId);
		map.put("stock", stock);
		map.put("lastModifiedDate", new Date());
		
		namedParameterJdbcTemplate.update(sqlB.toString(), map);
	}
	
	//刪除與ID對應的商品
	@Override
	public void deleteProductById(Integer productId) {
		StringBuilder sqlB3 = new StringBuilder();
		sqlB3.append("DELETE FROM product WHERE product_id = :productId ");
		
		Map<String, Object> map3 = new HashMap<>();
		map3.put("productId", productId);
		
		namedParameterJdbcTemplate.update(sqlB3.toString(), map3);
	}
	
	
	//查詢filter上面有要用到就可以直接call來用
	private StringBuilder addFilteringSql(StringBuilder sqlB, Map<String, Object> map, ProductQueryParams pqp) {
		
		if (pqp.getCategory() != null) {
			sqlB.append(" AND category = :category");  
			map.put("category", pqp.getCategory().name());
		}
		
		if (pqp.getSearch() != null) {
			sqlB.append(" AND product_name LIKE :search");  
			map.put("search", "%" + pqp.getSearch() + "%"); //LIKE搭配%(任意字符)使用%記得加在sql語法外
		}
		
		return sqlB;
	}

}