package com.example.demo.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.example.demo.dto.ProductQueryParams;
import com.example.demo.dto.UserRegisterRequest;
import com.example.demo.model.Product;
import com.example.demo.model.User;
import com.example.demo.rowmapper.UserRowMapper;

@Component
public class UserDaoImpl implements UserDao{

	@Autowired
	private NamedParameterJdbcTemplate nPJT;

	@Override
	public User getUserById(Integer userId) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("SELECT user_id, email, password, created_date, last_modified_date");
		sqlB.append(" FROM user WHERE user_id = :userId");
		
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		
		
		List<User> userList = nPJT.query(sqlB.toString(), map, new UserRowMapper());
		
		if(userList.size() > 0) {
			return userList.get(0);
		} else {
			return null;
		}
	}
	
	
	@Override
	public User getUserByEmail(String email) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("SELECT user_id, email, password, created_date, last_modified_date");
		sqlB.append(" FROM user WHERE email =:email");
		
		Map<String, Object> map = new HashMap<>();
		map.put("email", email);
		
		List<User> userList = nPJT.query(sqlB.toString(), map, new UserRowMapper());
		
		if(userList.size() > 0) {
			return userList.get(0);
		} else {
			return null;
		}
	}
	
	
	@Override
	public Integer createUser(UserRegisterRequest uRR) {
		StringBuilder sqlB = new StringBuilder();
		sqlB.append("INSERT INTO user(email, password, created_date, last_modified_date)");
		sqlB.append(" VALUES (:email, :password, :createdDate, :lastModifiedDate)");
		
		Map<String, Object> map = new HashMap<>(); //把email,password資料放進map
		map.put("email", uRR.getEmail());
		map.put("password", uRR.getPassWord());
		
		Date now = new Date();  
		map.put("createdDate", now);
		map.put("lastModifiedDate", now);
		

		KeyHolder keyHolder = new GeneratedKeyHolder();  //接住sql自動生成的userid
		
		nPJT.update(sqlB.toString(), new MapSqlParameterSource(map), keyHolder);
		
		int userId = keyHolder.getKey().intValue();
		
		return userId;
	}
}
