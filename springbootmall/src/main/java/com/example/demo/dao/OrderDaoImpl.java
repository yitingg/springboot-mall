package com.example.demo.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.example.demo.dto.OrderQueryParams;
import com.example.demo.dto.ProductQueryParams;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.rowmapper.OrderItemRowMapper;
import com.example.demo.rowmapper.OrderRowMapper;

@Component
public class OrderDaoImpl implements OrderDao { //Dao只跟資料庫溝通

	@Autowired
	private NamedParameterJdbcTemplate nPJT;
	
	public Integer countOrder(OrderQueryParams oQP) {
		
		StringBuilder StrB = new StringBuilder();
		StrB.append("SELECT count(*) FROM `order` WHERE 1=1");
		
		Map<String, Object> map = new HashMap<>();
		
		//查詢條件
		StrB = addFilteringSql(StrB, map, oQP);

		
		Integer total = nPJT.queryForObject(StrB.toString(), map, Integer.class);
		
		return total;
	}
	
	public List<Order> getOrders(OrderQueryParams oQP){
		
		StringBuilder StrB = new StringBuilder();
		StrB.append("SELECT order_id, user_id, total_amount, created_date, last_modified_date FROM `order` WHERE 1=1");
		
		Map<String, Object> map = new HashMap<>();
		
		//查詢條件
		StrB = addFilteringSql(StrB, map, oQP); //addFilteringSql專門處理 sql 查詢條件的拼接
		
		//排序
		StrB.append(" ORDER BY created_date DESC"); //直接寫死訂單紀錄不像商品需要調整
		
		//分頁
		StrB.append(" LIMIT :limit OFFSET :offset"); //決定要取的是幾道幾筆的數據
		map.put("limit", oQP.getLimit());
		map.put("offset", oQP.getOffset());
		
		List<Order> orderList = nPJT.query(StrB.toString(), map, new OrderRowMapper());
		
		return orderList;
	}
	
	
	
	@Override
	public Order getOrderById(Integer orderId) {
		StringBuilder StrB = new StringBuilder();
		StrB.append("SELECT order_id, user_id, total_amount, created_date, last_modified_date ");
		StrB.append("FROM `order` WHERE order_id = :orderId");
		
		Map<String, Object> map = new HashMap<>();
		map.put("orderId", orderId);
		
		List<Order> orderList = nPJT.query(StrB.toString(), map, new OrderRowMapper());
		
		if (orderList.size() > 0) {
			return orderList.get(0);
		}else {
			return null;
		}
	}
	
	@Override
	public List<OrderItem> getOrderItemsByOrderId(Integer orderId){
		StringBuilder StrB = new StringBuilder();
		StrB.append("SELECT oi.order_item_id, oi.order_id, oi.product_id, oi.quantity, oi.amount, p.product_name, p.image_url ");
		StrB.append("FROM order_item as oi ");
		StrB.append("LEFT JOIN product as p ON oi.product_id = p.product_id ");
		StrB.append("WHERE oi.order_id = :orderId");
		
		Map<String, Object> map = new HashMap<>();
		map.put("orderId", orderId);
		
		List<OrderItem> orderItemList = nPJT.query(StrB.toString(), map, new OrderItemRowMapper());
		
		return orderItemList;
	}
	
	
	
	@Override
	public Integer createOrder(Integer userId, Integer totalAmount) {
		StringBuilder StrB = new StringBuilder();
		StrB.append("INSERT INTO `order`(user_id, total_amount, created_date, last_modified_date) "); //`order`規避資料庫語法
		StrB.append("VALUES (:userId, :totalAmount, :createDate, :lastModifiedDate)");
		
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("totalAmount", totalAmount);
		
		Date now = new Date();
		map.put("createDate", now);
		map.put("lastModifiedDate", now);
		
		KeyHolder kH = new GeneratedKeyHolder();
		
		nPJT.update(StrB.toString(),new MapSqlParameterSource(map), kH);
		
		int orderId = kH.getKey().intValue();
		
		return orderId;
	}
	
	
	@Override
	public void createOrderItems(Integer orderId, List<OrderItem> orderItemList) {
		
//		// 使用 for 一條一條加入數據 => 效率較低
//		for (OrderItem orderItem : orderItemList) {
//			
//			StringBuilder StrB = new StringBuilder();
//			StrB.append("INSERT INTO order_item(order_id, product_id, quanttity, amount "); 
//			StrB.append("VALUES (:orderId, :productId, :quantity, :amount");
//			
//			Map<String, Object> map = new HashMap<>();
//			map.put("orderId", orderId);
//			map.put("productId", orderItem.getProductId());
//			map.put("quantity",orderItem.getQuantity());
//			map.put("amount", orderItem.getAmount());
//			
//			nPJT.update(StrB.toString(),map);
//		}
		
		
		// 使用 batchUpdate 一次性加入數據 => 效率更高 
		StringBuilder StrB = new StringBuilder();
		StrB.append("INSERT INTO order_item(order_id, product_id, quantity, amount) "); 
		StrB.append("VALUES (:orderId, :productId, :quantity, :amount)");
		
		MapSqlParameterSource[] pS = new MapSqlParameterSource[orderItemList.size()];
		
		for( int i = 0; i < orderItemList.size(); i++) {
			OrderItem orderItem = orderItemList.get(i);
			
			pS[i] = new MapSqlParameterSource();
			pS[i].addValue("orderId", orderId);
			pS[i].addValue("productId", orderItem.getProductId());
			pS[i].addValue("quantity", orderItem.getQuantity());
			pS[i].addValue("amount", orderItem.getAmount());
		}
		
		nPJT.batchUpdate(StrB.toString(), pS);
	}
	
	private StringBuilder addFilteringSql(StringBuilder StrB, Map<String, Object> map, OrderQueryParams oQP) {
			
			if (oQP.getUserId() != null) {
				StrB.append(" AND user_id = :userId");  
				map.put("userId", oQP.getUserId());
			}
			
			return StrB;
}
	}
