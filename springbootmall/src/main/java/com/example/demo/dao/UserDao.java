package com.example.demo.dao;

import com.example.demo.dto.UserRegisterRequest;
import com.example.demo.model.User;

public interface UserDao {

	User getUserById(Integer userId); // getUserById使用參數userId  返回類型User
	
	User getUserByEmail(String email);
	
	Integer createUser(UserRegisterRequest uRR);
}
