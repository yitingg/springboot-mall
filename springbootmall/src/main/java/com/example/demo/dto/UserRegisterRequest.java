package com.example.demo.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserRegisterRequest {

	@NotBlank  //@NotBlank:不能是null也不能是一個空白的字串
	@Email     //@Email檢查是否為email格式
	private String email;
	
	@NotBlank
	private String password;
	
	public String getEmail() {return email;};
	
	public void serEmail(String email) {this.email = email;}
	
	public String getPassWord() {return password;}
	
	public void setPassword(String password) {this.password = password;}
	
}
