package com.example.demo.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BuyItem {  //buyItemList中的值

	@NotNull
	private Integer productId;
	
	@NotNull
	private Integer quantity;
}
