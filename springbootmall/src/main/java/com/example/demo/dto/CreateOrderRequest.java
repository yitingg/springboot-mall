package com.example.demo.dto;

import java.util.List;

import jakarta.validation.constraints.NotEmpty;

public class CreateOrderRequest {

	@NotEmpty
	private List<BuyItem> buyItemList; //對應前端postman所傳回來Key的值 巢狀Json寫法4-4
	
	public List<BuyItem> getBuyItemList(){
		return buyItemList;
		
	}
	
	public void setBuyItemList(List<BuyItem> buyItemList) {
		this.buyItemList = buyItemList;
	}
	
}
