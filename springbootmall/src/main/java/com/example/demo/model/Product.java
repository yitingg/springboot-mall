package com.example.demo.model;

import java.sql.Timestamp;
import java.util.Date;

import com.example.demo.constant.ProductCategory;

import lombok.Data;


@Data
public class Product {

	private Integer productId;
	private String productName;
//	private String category;
	private ProductCategory category;
	private String imageUrl;
	private Integer price;
	private Integer stock;
	private String description;
	private Date createDate; //Date類型預設使用GMT+0,timestamp(時間戳)使用UTC=GMT+0,不管英國台灣都會是一樣的整數
	private Date lastModifiedDate; //在java中可以使用System.currentTimeMillis()取得當前Timestamp

}
