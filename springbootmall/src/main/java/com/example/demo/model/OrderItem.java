package com.example.demo.model;

import lombok.Data;

@Data
public class OrderItem {

	//原有的
	private Integer orderItemId;
	private Integer orderId;
	private Integer productId;
	private Integer quantity;
	private Integer amount;
	
	//有join到後來擴充的
	private String productName;
	private String imageUrl;
}
