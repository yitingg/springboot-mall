package com.example.demo.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


@Data
public class User {

	private Integer userId;
	
	@JsonProperty("e_mail") //會將postman中email的顯示格式變成e_mail
	private String email;
	
	@JsonIgnore //隱藏 password 在 postman 中的顯示
	private String password;
	
	private Date createdDate;
	private Date lastModifiedDate;
}
