package com.example.demo.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Order {

	
	private Integer orderId;
	private Integer userId;
	private Integer totalAmount;
	private Date createDate;
	private Date lastModifiedDate;
	
	private List<OrderItem> orderItemList;
}
