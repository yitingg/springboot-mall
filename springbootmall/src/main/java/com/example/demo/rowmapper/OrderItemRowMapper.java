package com.example.demo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.model.OrderItem;

public class OrderItemRowMapper implements RowMapper<OrderItem>{

	@Override
	public OrderItem mapRow(ResultSet rs, int i) throws SQLException{
		OrderItem oi = new OrderItem();
		
		//本來就存在 orderitem 中的欄位
		oi.setOrderItemId(rs.getInt("order_item_id"));
		oi.setOrderId(rs.getInt("order_id"));
		oi.setProductId(rs.getInt("product_id"));
		oi.setQuantity(rs.getInt("quantity"));
		oi.setAmount(rs.getInt("amount"));
		
		
		//對應 DaoImpl 中 getOrderItemsByOrderId 的 LEFT JOIN product as p ON oi.product_id = p.product_id 
		//要回去 model.orderItem 中加上打算擴充的
		oi.setProductName(rs.getString("product_name"));
		oi.setImageUrl(rs.getString("image_url"));
		
		return oi;
	}
	
}
