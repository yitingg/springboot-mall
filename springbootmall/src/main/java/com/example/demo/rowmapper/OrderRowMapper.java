package com.example.demo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.model.Order;

public class OrderRowMapper implements RowMapper<Order>{
	
	@Override
	public Order mapRow(ResultSet rS, int i) throws SQLException{
		Order order = new Order();
		order.setOrderId(rS.getInt("order_id"));
		order.setOrderId(rS.getInt("user_id"));
		order.setTotalAmount(rS.getInt("total_amount"));
		order.setCreateDate(rS.getTimestamp("created_date"));
		order.setLastModifiedDate(rS.getTimestamp("last_modified_date"));
		
		return order;
	}

}
