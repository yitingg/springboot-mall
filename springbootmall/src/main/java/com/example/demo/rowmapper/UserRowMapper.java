package com.example.demo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.model.User;

public class UserRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet resultSet, int i) throws SQLException{
		
		User us = new User();
		us.setUserId(resultSet.getInt("user_id"));
		us.setEmail(resultSet.getString("email"));
		us.setPassword(resultSet.getString("password"));
		us.setCreatedDate(resultSet.getTimestamp("created_date"));
		us.setLastModifiedDate(resultSet.getTimestamp("last_modified_date"));

		return us;
	}
}
