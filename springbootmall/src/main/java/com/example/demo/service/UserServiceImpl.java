package com.example.demo.service;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.dao.UserDao;
import com.example.demo.dto.UserLoginRequest;
import com.example.demo.dto.UserRegisterRequest;
import com.example.demo.model.User;

@Component
public class UserServiceImpl implements UserService{
	
	private final static org.slf4j.Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public User getUserById(Integer userId) {
		return userDao.getUserById(userId);
	}
	
	@Override
	public Integer register(UserRegisterRequest uRR) {
		//檢查註冊的email
		User user = userDao.getUserByEmail(uRR.getEmail());
		
		if(user != null) {
			log.warn("該 email {} 已經被註冊", uRR.getEmail());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} 
		
		//使用 MD5 生成註冊密碼的雜湊值
		String hashedPw = DigestUtils.md5DigestAsHex(uRR.getPassWord().getBytes());
		uRR.setPassword(hashedPw);
		
		
		//創建帳號
		return userDao.createUser(uRR); 
	}
	
	@Override
	public User login(UserLoginRequest uLR) {
		//先使用 Dao 層的getUserByEmail方法，前端傳過來Email都值去資料庫中查詢user 數據出來
		User user = userDao.getUserByEmail(uLR.getEmail());
		
		
		//檢查user是否存在
		if(user == null) {
			log.warn("該 email {} 尚未註冊", uLR.getEmail()  );
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		//使用 MD5 生成登入密碼的雜湊值
		String hashedPw = DigestUtils.md5DigestAsHex(uLR.getPassword().getBytes());
		
		
		//比較密碼 if user在資料庫中所存儲的password的值 = 前端所回傳過來的 password 的值的話
		if (user.getPassword().equals(uLR.getPassword())) {
			return user;
		}else {
			log.warn("email {} 的密碼不正確", uLR.getEmail());
			//强制停止前端的請求
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

}
