package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.dao.OrderDao;
import com.example.demo.dao.UserDao;
import com.example.demo.dao.productDao;
import com.example.demo.dto.BuyItem;
import com.example.demo.dto.CreateOrderRequest;
import com.example.demo.dto.OrderQueryParams;
import com.example.demo.model.Order;
import com.example.demo.model.OrderItem;
import com.example.demo.model.Product;
import com.example.demo.model.User;

@Component
public class OrderServiceImpl implements OrderService{ //service注入多個Dao的bean幫助達成目標

	//因下面createorder有用到log做紀錄
	private final static Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private productDao productDao;
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public Integer countOrder(OrderQueryParams oQP) {
		return orderDao.countOrder(oQP);
	}
	
	@Override
	public List<Order> getOrders(OrderQueryParams oQP){
		List<Order> orderList = orderDao.getOrders(oQP);
		
		for(Order order : orderList) {
			List<OrderItem> orderItemList = orderDao.getOrderItemsByOrderId(order.getOrderId());
			
			order.setOrderItemList(orderItemList);
		}
		
		return orderList;
	}
	
	@Override
	public Order getOrderById(Integer orderId) {
		Order order = orderDao.getOrderById(orderId);
		
		List<OrderItem> orderItemList = orderDao.getOrderItemsByOrderId(orderId);
		
		order.setOrderItemList(orderItemList);
		
		return order;
	}
	
	@Transactional 
	@Override
	// 正式createOrder之前 檢查 user/product 是否存在 , 商品庫存是否足夠 
	//@Transactional萬一中間有出錯就撤回全部操作 確保兩個table可以同時新增成功/失敗
	public Integer createOrder(Integer userId, CreateOrderRequest createOrderRequest) {
		// 檢查 user 是否存在
		User user = userDao.getUserById(userId);
		
		if (user == null) {
			log.warn("該 userId {} 不存在", userId);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		int totalAmount = 0;
		List<OrderItem> orderItemList = new ArrayList<>();
		
		for(BuyItem bI : createOrderRequest.getBuyItemList()) {
			Product p = productDao.getProductById(bI.getProductId());
			
			//檢查product是否存在、庫存是否足夠
			if (p == null) {
				log.warn("商品 {} 不存在", bI.getProductId());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				
			}else if (p.getStock() < bI.getQuantity()){
				log.warn("商品 {} 庫存不足無法購買，剩餘庫存 {} ，欲購買數量 {} ", bI.getProductId(), p.getStock(), bI.getQuantity());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}
			
			//確認product存在且庫存足夠後 扣除商品庫存
			productDao.updateStock(p.getProductId(), p.getStock() - bI.getQuantity());
			
			//計算總價格
			int amount = bI.getQuantity() * p.getPrice();
			totalAmount = totalAmount + amount;
			
			//轉換 BuyItem to OrderItem
			OrderItem orderItem = new OrderItem();
			orderItem.setProductId(bI.getProductId());
			orderItem.setQuantity(bI.getQuantity());
			orderItem.setAmount(amount);
			
			orderItemList.add(orderItem);
		}
		
		//創建訂單
		Integer orderId = orderDao.createOrder(userId, totalAmount);
		
		orderDao.createOrderItems(orderId, orderItemList);
		
		return orderId;
	}
}
