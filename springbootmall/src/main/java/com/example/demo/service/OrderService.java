package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.CreateOrderRequest;
import com.example.demo.dto.OrderQueryParams;
import com.example.demo.model.Order;

public interface OrderService {
	
	Integer countOrder(OrderQueryParams oQP);
	
	List<Order> getOrders(OrderQueryParams oQP);
	
	Order getOrderById(Integer orderId);

	Integer createOrder(Integer userId, CreateOrderRequest createOrderRequest);
}
