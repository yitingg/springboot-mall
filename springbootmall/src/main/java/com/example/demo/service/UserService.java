package com.example.demo.service;

import com.example.demo.dto.UserLoginRequest;
import com.example.demo.dto.UserRegisterRequest;
import com.example.demo.model.User;

public interface UserService {

	User getUserById(Integer userId);
	
	Integer register(UserRegisterRequest uRR);
	
	//返回值 方法名稱 參數
	User login(UserLoginRequest uLR);
}
